import React, { Component } from "react";
import UploadFile  from "./services/uploadFile";


class Index extends Component {

    render() {
        return (
            <React.Fragment>
                <div className="ml-lg-5 mt-lg-5 mb-0">
                <h4>Nepali-English OCR</h4>
                <p className="text-justify">This OCR tool can analyze the text in any image file that you upload, and then convert the text from the image into text that you can easily edit on your computer.</p>
                </div>
            <div className="container-fluid align-content-center overlay mt-1">
                <UploadFile/>
            </div>
            </React.Fragment>
        );

    }
}
export default Index;
