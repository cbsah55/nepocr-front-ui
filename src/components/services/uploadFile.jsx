import React, {Component} from 'react';
import uploadFileService from "./uploadFileService";
import DisplayResponse from "./displayResponse";
class UploadFile extends Component {
    state = {
        currentFile: undefined,
        progress: 0,
        message: "",
        lang: "nep",
        response: "",
        fileUrl:"",
        fileName:""
    };

    selectFile = (e) => {
        e.preventDefault();
        this.setState({
            currentFile: e.target.files[0]
        });
        this.setState({
            fileUrl: URL.createObjectURL(e.target.files[0])
        });
        this.setState({
            fileName: e.target.files[0].name
        });
    };
    handleUpload = async () => {
        console.log(this.state.lang);
        const response = await uploadFileService.upload(this.state.currentFile, this.state.progress, this.state.lang);
        this.setState({
            response: response
        });

    };

    selectLanguage=(e)=>{
        if (e.target.value !== "")
        this.setState({
            lang : e.target.value
        });
    };

    render() {
        return (
            // eslint-disable-next-line react/style-prop-object
            <React.Fragment>
                <div className="row">
                    <div className="col-sm-6">
                <div className=" shadow p-3 mb-5 bg-white rounded">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title font-weight-bold">Choose the file</h4>
                            <div className="row">
                                <div className="col">
                                    <label htmlFor="files" className="btn btn-primary">Select File</label>
                                <input type="file" id="files" accept=".png, .jpg, .jpeg, .pdf" style={{display:"none"}} onChange={this.selectFile}/>
                                </div>
                                <div className="col">
                                    <select id = "language" className="form-control" onChange={this.selectLanguage}>
                                        <option value="">Select Language</option>
                                        <option value="eng">English</option>
                                        <option value="nep">Nepali</option>

                                    </select>
                                </div>
                                <div className="col d-flex justify-content-center">
                                <button className="btn btn-info mb-2" disabled={!this.state.currentFile} onClick={this.handleUpload}>Upload</button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <p>File Selected : {this.state.fileName}</p>
                                </div>
                            </div>
                            <img src={this.state.fileUrl} className="card-img-bottom" alt=" "/>
                        </div>
                    </div>
                </div>
                    </div>
                    <div className="col">
                <DisplayResponse data={this.state.response}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default UploadFile;
