import http from "../api/http-common"

class UploadFileService {
    async upload(file, onUploadProgress, lang) {
        let formData = new FormData;

        formData.append("file", file);
        formData.append("lang", lang);

        try {
            const {data: response} = await http.post("/uploadFile", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return response;
        } catch (error) {

        }
    }
}
export default new UploadFileService();