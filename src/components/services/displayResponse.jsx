import React, {Component} from 'react';
import SunEditor from "suneditor-react";

class DisplayResponse extends Component {


    render() {
        return (
            <React.Fragment>
                <div className=" shadow p-3 mb-5 bg-white rounded">
                    <div className="card">
                        <div className="card-body" style={{height: 390+'px'}}>
                           <SunEditor setContents={this.props.data} height="250px" setDefaultStyle="font-size: 20px;"/>
                           </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default DisplayResponse;